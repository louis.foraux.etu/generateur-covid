<!doctype html>
<html lang="fr">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">

    <title>Générateur d'attestation de déplacement dérogatoire Covid-19</title>
</head>
<body>

<div class="container-fluid">
    <div class="row text-center">
        <h1>Vous possédez un code d'une précédente identification</h1>
        <form action="process/verify.php" method="post">
            <input type="number" name="code" placeholder="Votre code" class="form-control" />
            <input type="submit" value="Envoyer" class="btn btn-primary" />
        </form>
    </div>
</div>


<!-- JAVASCRIPT pour Bootstrap -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/js/bootstrap.bundle.min.js" integrity="sha384-BOsAfwzjNJHrJ8cZidOg56tcQWfp6y72vEJ8xQ9w6Quywb24iOsW913URv1IS4GD" crossorigin="anonymous"></script>

</body>
</html>