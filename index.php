<!doctype html>
<html lang="fr">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/css/bootstrap.min.css" integrity="sha384-DhY6onE6f3zzKbjUPRc2hOzGAdEf4/Dz+WJwBvEYL/lkkIsI3ihufq9hk9K4lVoK" crossorigin="anonymous">

    <title>Générateur d'attestation de déplacement dérogatoire Covid-19</title>
</head>
<body>

<div class="container-fluid">
    <div class="row text-center">
        <h1>Générateur de déplacement dérogatoire pour l'IUT A</h1>
        <h3>Deux choix s'offrent alors à vous :</h3>
        <a href="nocode.php" class="btn btn-default">Je ne possède pas de code</a>
        <h4>OU</h4>
        <a href="code.php" class="btn btn-primary">Je possède un code d'identification</a>
    </div>
</div>


<!-- JAVASCRIPT pour Bootstrap -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha2/js/bootstrap.bundle.min.js" integrity="sha384-BOsAfwzjNJHrJ8cZidOg56tcQWfp6y72vEJ8xQ9w6Quywb24iOsW913URv1IS4GD" crossorigin="anonymous"></script>

</body>
</html>